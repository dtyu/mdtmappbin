# These are the default value for all archetures. The makefile appears
# cumbersome but only because we wanted to have one makefile that would
# work in a variety of disparate architecture and various release levels.
# Additionally, this make file is easily modifiable for test environments.
#

BINDIR     = ../bin/$(OSVER)

OBJDIR     = ../obj/$(OSVER)

SRC1	   = ./mdtmApp

SRC2	   = ./bbcp_modified

SRC3	   = ./bbcp_original

ENVCFLAGS  =    -Dunix -D_BSD -D_ALL_SOURCE -g

ENVINCLUDE = -I/usr/local/include -I/usr/include \
			 -I./bbcp_original -I./bbcp_modified -I./mdtmApp 

AIXLIBS    =  -q64 -L/usr/lib -lz -lpthreads -g -laio

BSDLIBS    =  -L/usr/lib -lz -pthread -g -laio 


LNXLIBS32  =  -lnsl -lpthread -laio -lrt -lz -lnuma -g  
LNXLIBS64  =  -lnsl -lpthread -laio -lrt -lz -lnuma -g  
LNXLIBSMDTM  =  -lnsl -lpthread -laio -lrt -lz -lnuma -g -lmdtm 

MACLIBS    =  -dynamic -L/usr/lib -laio -lz -g -lpthread

SUNLIBSA   =  /usr/local/lib/libz.a -lposix4 -lsocket -lnsl -lpthread -g -laio
SUNLIBS    =  -L/usr/lib -lz -lposix4 -lsocket -lnsl -lpthread -g -laio

MKPARMS  = doitall

NLGR       = -DNL_THREADSAFE

SUN64      = -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
SUNMT      = -D_REENTRANT -DOO_STD -mt
SUNOPT     = -g -fast $(SUNMT) $(SUN64) $(NLGR) -DSUN

SUNCC      = CC
SUNcc      = cc

S86OPT     = -D_REENTRANT -DOO_STD $(NLGR) -DSUN -DSUNX86 -Wno-deprecated
S86OPT     = $(SUNOPT) -DSUNX86 

S86CC      = CC
S86cc      = gcc

SUNGCC64   = -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
SUNGCCMT   = -D_REENTRANT -DOO_STD
SUNGCCOPT  = -L/usr/local/lib -g \
             $(SUNGCCMT) $(SUNGCC64) $(NLGR) -DSUN

SUNGCC     = g++
SUNGcc     = gcc

LNXOPT     = $(SUN64) $(NLGR) -D_REENTRANT -DOO_STD -DLINUX -Wno-deprecated \
             -D_GNU_SOURCE 
LNXOPT_B   = $(SUN64) $(NLGR) -D_REENTRANT -DOO_STD -DLINUX \
             -D_GNU_SOURCE 

LNXCC      = g++
LNXcc      = gcc

BSDOPT     = $(SUN64) $(NLGR) -D_REENTRANT -DOO_STD -DFREEBSD -pthread

BSDCC      = g++
BSDcc      = gcc

MACOPT     = $(SUN64) $(NLGR) -D_REENTRANT -DOO_STD -DMACOS -Wno-deprecated -g

MACCC      = g++
MACcc      = gcc

AIXCC      = xlC
AIXcc      = xlc

AIXOPT     = -q64 $(SUN64) $(NLGR) -D_REENTRANT -DOO_STD -DAIX -g

SOURCE =      \
     $(SRC1)/mdtmAppA.C \
     $(SRC1)/mdtmAppD.C \
     $(SRC2)/bbcp_Args.C \
     $(SRC2)/bbcp_BuffPool.C \
     $(SRC3)/bbcp_C32.C \
     $(SRC3)/bbcp_ChkSum.C \
     $(SRC2)/bbcp_Config.C \
     $(SRC3)/bbcp_Emsg.C \
     $(SRC2)/bbcp_File.C \
     $(SRC2)/bbcp_FileSpec.C \
     $(SRC2)/bbcp_FileSystem.C \
     $(SRC2)/bbcp_FS_Null.C \
     $(SRC2)/bbcp_FS_Pipe.C \
     $(SRC2)/bbcp_FS_Unix.C \
     $(SRC2)/bbcp_IO.C \
     $(SRC2)/bbcp_IO_Null.C \
     $(SRC2)/bbcp_IO_Pipe.C \
     $(SRC2)/bbcp_Link.C \
     $(SRC2)/bbcp_LogFile.C \
     $(SRC3)/bbcp_MD5.C \
     $(SRC2)/bbcp_NetLogger.C \
     $(SRC2)/bbcp_Network.C \
     $(SRC2)/bbcp_Node.C \
     $(SRC2)/bbcp_ProcMon.C \
     $(SRC2)/bbcp_ProgMon.C \
     $(SRC2)/bbcp_Protocol.C \
     $(SRC2)/bbcp_Pthread.C \
     $(SRC3)/bbcp_RTCopy.C \
     $(SRC2)/bbcp_Stream.C \
     $(SRC2)/bbcp_System.C \
     $(SRC2)/bbcp_Timer.C \
     $(SRC3)/bbcp_Version.C \
     $(SRC1)/MDTMApp_Network.C \
     $(SRC1)/MDTMApp_TaskGroup.C \
     $(SRC1)/MDTMApp_Daemon.C \
     $(SRC3)/bbcp_ZCX.C \
     $(SRC3)/NetLogger.c

OBJECTA =      \
     $(OBJDIR)/mdtmAppA.o \
     $(OBJDIR)/bbcp_Args.o \
     $(OBJDIR)/bbcp_BuffPool.o \
     $(OBJDIR)/bbcp_C32.o \
     $(OBJDIR)/bbcp_ChkSum.o \
     $(OBJDIR)/bbcp_Config.o \
     $(OBJDIR)/bbcp_Emsg.o \
     $(OBJDIR)/bbcp_File.o \
     $(OBJDIR)/bbcp_FileSpec.o \
     $(OBJDIR)/bbcp_FileSystem.o \
     $(OBJDIR)/bbcp_FS_Null.o \
     $(OBJDIR)/bbcp_FS_Pipe.o \
     $(OBJDIR)/bbcp_FS_Unix.o \
     $(OBJDIR)/bbcp_IO.o \
     $(OBJDIR)/bbcp_IO_Null.o \
     $(OBJDIR)/bbcp_IO_Pipe.o \
     $(OBJDIR)/bbcp_Link.o \
     $(OBJDIR)/bbcp_LogFile.o \
     $(OBJDIR)/bbcp_MD5.o \
     $(OBJDIR)/bbcp_NetLogger.o \
     $(OBJDIR)/bbcp_Network.o \
     $(OBJDIR)/bbcp_Node.o \
     $(OBJDIR)/bbcp_ProcMon.o \
     $(OBJDIR)/bbcp_ProgMon.o \
     $(OBJDIR)/bbcp_Protocol.o \
     $(OBJDIR)/bbcp_Pthread.o \
     $(OBJDIR)/bbcp_RTCopy.o \
     $(OBJDIR)/bbcp_Stream.o \
     $(OBJDIR)/bbcp_System.o \
     $(OBJDIR)/bbcp_Timer.o \
     $(OBJDIR)/bbcp_Version.o \
     $(OBJDIR)/MDTMApp_Network.o \
     $(OBJDIR)/MDTMApp_TaskGroup.o \
     $(OBJDIR)/MDTMApp_Daemon.o \
     $(OBJDIR)/bbcp_ZCX.o \
     $(OBJDIR)/NetLogger.o

OBJECTB =      \
     $(OBJDIR)/mdtmAppD.o \
     $(OBJDIR)/bbcp_Args.o \
     $(OBJDIR)/bbcp_BuffPool.o \
     $(OBJDIR)/bbcp_C32.o \
     $(OBJDIR)/bbcp_ChkSum.o \
     $(OBJDIR)/bbcp_Config.o \
     $(OBJDIR)/bbcp_Emsg.o \
     $(OBJDIR)/bbcp_File.o \
     $(OBJDIR)/bbcp_FileSpec.o \
     $(OBJDIR)/bbcp_FileSystem.o \
     $(OBJDIR)/bbcp_FS_Null.o \
     $(OBJDIR)/bbcp_FS_Pipe.o \
     $(OBJDIR)/bbcp_FS_Unix.o \
     $(OBJDIR)/bbcp_IO.o \
     $(OBJDIR)/bbcp_IO_Null.o \
     $(OBJDIR)/bbcp_IO_Pipe.o \
     $(OBJDIR)/bbcp_Link.o \
     $(OBJDIR)/bbcp_LogFile.o \
     $(OBJDIR)/bbcp_MD5.o \
     $(OBJDIR)/bbcp_NetLogger.o \
     $(OBJDIR)/bbcp_Network.o \
     $(OBJDIR)/bbcp_Node.o \
     $(OBJDIR)/bbcp_ProcMon.o \
     $(OBJDIR)/bbcp_ProgMon.o \
     $(OBJDIR)/bbcp_Protocol.o \
     $(OBJDIR)/bbcp_Pthread.o \
     $(OBJDIR)/bbcp_RTCopy.o \
     $(OBJDIR)/bbcp_Stream.o \
     $(OBJDIR)/bbcp_System.o \
     $(OBJDIR)/bbcp_Timer.o \
     $(OBJDIR)/bbcp_Version.o \
     $(OBJDIR)/MDTMApp_Network.o \
     $(OBJDIR)/MDTMApp_TaskGroup.o \
     $(OBJDIR)/MDTMApp_Daemon.o \
     $(OBJDIR)/bbcp_ZCX.o \
     $(OBJDIR)/NetLogger.o

TARGETA  = $(BINDIR)/mdtmApp 
TARGETB  = $(BINDIR)/mdtmAppD

all:
#	@make `uname` OSVER=`../MakeSname`
	@echo Make done.

#doitall: $(TARGETA) $(TARGETB)

#clean:
#	@make cleanall OSVER=`../MakeSname`

#cleanall:
#	@rm -rf $(OBJECTA) $(OBJECTB) $(BINDIR)/core $(TARGETA) $(TARGETB) $(OBJDIR)/* ../bin/* ../obj/*

install:
	@make installall 

installall:
	@cp mdtmApp /usr/local/bin
	@cp mdtmApp /sbin
	@echo Install done.

install-local:
	@./InstallLocal.sh > /dev/null 2>&1
	@echo Local install done.

usage:
	@echo "Usage: make [install | install-local]"

echo:		
	@for f in $(SOURCE); do \
	echo $$f ;\
	done

AIX:
	@make $(MKPARMS) \
	CC=$(AIXCC) \
	BB=$(AIXcc) \
	BFLAGS="$(ENVCFLAGS) $(AIXOPT)" \
	CFLAGS="$(ENVCFLAGS) $(AIXOPT)" \
	INCLUDE="$(ENVINCLUDE)"  \
	LIBS="$(AIXLIBS)"

FreeBSD:
	@make $(MKPARMS)  \
	CC=$(BSDCC) \
	BB=$(BSDcc) \
	CFLAGS="$(ENVCFLAGS) $(BSDOPT) $(OPT)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(BSDLIBS)"

Linux:
	@make makeLinux`/bin/uname -i`

makeLinuxi386:
	@make $(MKPARMS)  \
	CC=$(LNXCC) \
	BB=$(LNXcc) \
	CFLAGS="$(ENVCFLAGS) $(LNXOPT)" \
	BFLAGS="$(ENVCFLAGS) $(LNXOPT_B)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(LNXLIBS32)"

makeLinuxx86_64:
	@make $(MKPARMS)  \
	CC=$(LNXCC) \
	BB=$(LNXcc) \
	CFLAGS="$(ENVCFLAGS) $(LNXOPT)" \
	BFLAGS="$(ENVCFLAGS) $(LNXOPT_B)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(LNXLIBS64)"

MDTM:
	@make $(MKPARMS) OSVER=`../MakeSname` \
	CC=$(LNXCC) \
	BB=$(LNXcc) \
	CFLAGS="$(ENVCFLAGS) -DMDTM $(LNXOPT)" \
	BFLAGS="$(ENVCFLAGS) $(LNXOPT_B)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(LNXLIBSMDTM)"

Darwin:
	@MACOSX_DEPLOYMENT_TARGET=10.5;\
	@make $(MKPARMS)  \
	CC=$(MACCC) \
	BB=$(MACcc) \
	CFLAGS="$(ENVCFLAGS) $(MACOPT) $(OPT)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(MACLIBS)"

UNICOS/mp:
	@make $(MKPARMS)  \
 	CC=$(LNXCC) \
 	BB=$(LNXcc) \
 	CFLAGS="$(ENVCFLAGS) $(LNXOPT)" \
	BFLAGS="$(ENVCFLAGS) $(LNXOPT_B)" \
 	INCLUDE="$(ENVINCLUDE)" \
 	LIBS="$(LNXLIBS)"

SunOS:
	@make $(MKPARMS)  \
	CC=$(SUNCC) \
	BB=$(SUNcc) \
	CFLAGS="$(ENVCFLAGS) $(SUNOPT) $(OPT)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(SUNLIBS)"

SunX86:
	@make $(MKPARMS)  \
	CC=$(S86CC) \
	BB=$(S86cc) \
	CFLAGS="$(ENVCFLAGS) $(S86OPT) $(OPT)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(SUNLIBS)"

Sungcc:
	@make $(MKPARMS)  \
	CC=$(SUNGCC) \
	BB=$(SUNGcc) \
	CFLAGS="$(ENVCFLAGS) $(SUNGCCOPT) $(OPT)" \
	INCLUDE="$(ENVINCLUDE)" \
	LIBS="$(SUNLIBS)" \
	OSVER=`../MakeSname`

$(TARGETA): $(OBJECTA)
	@echo Creating executable $(TARGETA) ...
	@$(CC) $(OBJECTA) $(LIBS) -o $(TARGETA)

$(TARGETB): $(OBJECTB)
	@echo Creating executable $(TARGETB) ...
	@$(CC) $(OBJECTB) $(LIBS) -o $(TARGETB)

$(OBJDIR)/mdtmAppA.o: $(SRC1)/mdtmAppA.C $(SRC2)/bbcp_Args.h \
                      $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                      $(SRC2)/bbcp_FileSpec.h $(SRC2)/bbcp_Link.h \
                      $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Node.h \
					  $(SRC2)/bbcp_Protocol.h $(SRC2)/bbcp_System.h \
                      $(SRC2)/bbcp_Headers.h $(SRC1)/MDTMApp.h \
                      $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp_TaskGroup.h \
                      $(SRC1)/MDTMApp_Daemon.h
	@echo Compiling mdtmAppA.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC1)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/mdtmAppD.o: $(SRC1)/mdtmAppD.C $(SRC2)/bbcp_Args.h \
                      $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                      $(SRC2)/bbcp_FileSpec.h $(SRC3)/bbcp_LogFile.h \
                      $(SRC2)/bbcp_Node.h $(SRC2)/bbcp_Protocol.h \
                      $(SRC2)/bbcp_System.h $(SRC2)/bbcp_Headers.h \
                      $(SRC2)/bbcp_Link.h $(SRC1)/MDTMApp_TaskGroup.h \
                      $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp_Daemon.h \
                      $(SRC1)/MDTMApp.h
	@echo Compiling mdtmAppD.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC1)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Args.o: $(SRC2)/bbcp_Args.C $(SRC2)/bbcp_Args.h \
					   $(SRC2)/bbcp_Config.h $(SRC2)/bbcp_Stream.h
	@echo Compiling bbcp_Args.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_BuffPool.o: $(SRC2)/bbcp_BuffPool.C $(SRC2)/bbcp_BuffPool.h \
						   $(SRC3)/bbcp_Debug.h $(SRC3)/bbcp_Emsg.h \
						   $(SRC2)/bbcp_Pthread.h $(SRC2)/bbcp_Headers.h
	@echo Compiling bbcp_BuffPool.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_C32.o: $(SRC3)/bbcp_C32.C $(SRC3)/bbcp_C32.h \
					  $(SRC3)/bbcp_ChkSum.h $(SRC3)/bbcp_Endian.h
	@echo Compiling bbcp_C32.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_ChkSum.o: $(SRC3)/bbcp_ChkSum.C $(SRC3)/bbcp_ChkSum.h \
                         $(SRC3)/bbcp_Endian.h $(SRC3)/bbcp_A32.h \
                         $(SRC3)/bbcp_C32.h $(SRC3)/bbcp_MD5.h
	@echo Compiling bbcp_ChkSum.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Config.o: $(SRC2)/bbcp_Config.C $(SRC2)/bbcp_Config.h \
                         $(SRC2)/bbcp_Args.h $(SRC3)/bbcp_Debug.h \
                         $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_FileSpec.h \
                         $(SRC3)/bbcp_LogFile.h $(SRC3)/bbcp_NetLogger.h \
                         $(SRC2)/bbcp_Network.h $(SRC2)/bbcp_Platform.h \
                         $(SRC2)/bbcp_Stream.h $(SRC2)/bbcp_System.h \
                         $(SRC3)/bbcp_Version.h $(SRC2)/bbcp_Headers.h \
                         $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_Config.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Emsg.o: $(SRC3)/bbcp_Emsg.C $(SRC3)/bbcp_Emsg.h \
                       $(SRC3)/bbcp_Debug.h $(SRC2)/bbcp_Platform.h \
                       $(SRC2)/bbcp_Headers.h
	@echo Compiling bbcp_Emsg.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_File.o: $(SRC2)/bbcp_File.C $(SRC2)/bbcp_File.h \
                       $(SRC2)/bbcp_IO.h $(SRC2)/bbcp_BuffPool.h \
                       $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                       $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Pthread.h \
                       $(SRC2)/bbcp_Platform.h $(SRC2)/bbcp_Headers.h \
                       $(SRC3)/bbcp_ChkSum.h $(SRC3)/bbcp_RTCopy.h
	@echo Compiling bbcp_File.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_FileSpec.o: $(SRC2)/bbcp_FileSpec.C $(SRC2)/bbcp_FileSpec.h \
                           $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Emsg.h \
                           $(SRC2)/bbcp_FileSystem.h $(SRC2)/bbcp_FS_Unix.h \
                           $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_FileSpec.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_FileSystem.o: $(SRC2)/bbcp_FileSystem.C $(SRC2)/bbcp_FileSystem.h \
                             $(SRC2)/bbcp_FS_Null.h $(SRC2)/bbcp_FS_Pipe.h \
                             $(SRC2)/bbcp_FS_Unix.h
	@echo Compiling bbcp_FileSystem.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_FS_Null.o: $(SRC2)/bbcp_FS_Null.C $(SRC2)/bbcp_FS_Null.h \
                          $(SRC2)/bbcp_IO_Null.h $(SRC2)/bbcp_FileSystem.h \
                          $(SRC2)/bbcp_System.h $(SRC2)/bbcp_Platform.h \
                          $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_FS_Null.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_FS_Pipe.o: $(SRC2)/bbcp_FS_Pipe.C $(SRC2)/bbcp_FS_Pipe.h \
                          $(SRC2)/bbcp_FS_Unix.h $(SRC2)/bbcp_IO_Pipe.h \
                          $(SRC2)/bbcp_FileSystem.h $(SRC2)/bbcp_System.h \
                          $(SRC2)/bbcp_Platform.h $(SRC2)/bbcp_Pthread.h \
                          $(SRC3)/bbcp_Emsg.h
	@echo Compiling bbcp_FS_Pipe.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_FS_Unix.o: $(SRC2)/bbcp_FS_Unix.C $(SRC2)/bbcp_FS_Unix.h \
                          $(SRC2)/bbcp_FileSystem.h $(SRC2)/bbcp_Config.h \
                          $(SRC2)/bbcp_IO.h $(SRC2)/bbcp_IO_Null.h \
                          $(SRC2)/bbcp_Platform.h $(SRC2)/bbcp_System.h \
                          $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_FS_Unix.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_IO.o: $(SRC2)/bbcp_IO.C $(SRC2)/bbcp_IO.h \
                     $(SRC3)/bbcp_NetLogger.h $(SRC2)/bbcp_Timer.h
	@echo Compiling bbcp_IO.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_IO_Null.o: $(SRC2)/bbcp_IO_Null.C $(SRC2)/bbcp_IO_Null.h \
                          $(SRC2)/bbcp_IO.h
	@echo Compiling bbcp_IO_Null.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_IO_Pipe.o: $(SRC2)/bbcp_IO_Pipe.C $(SRC2)/bbcp_IO_Pipe.h $(SRC2)/bbcp_IO.h \
                          $(SRC2)/bbcp_System.h
	@echo Compiling bbcp_IO_Pipe.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Link.o: $(SRC2)/bbcp_Link.C $(SRC2)/bbcp_Link.h \
			           $(SRC2)/bbcp_File.h $(SRC2)/bbcp_IO.h \
                       $(SRC2)/bbcp_BuffPool.h $(SRC3)/bbcp_ChkSum.h \
                       $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                       $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Network.h \
                       $(SRC2)/bbcp_Platform.h $(SRC2)/bbcp_Timer.h
	@echo Compiling bbcp_Link.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_LogFile.o:  $(SRC3)/bbcp_LogFile.C $(SRC3)/bbcp_LogFile.h \
						   $(SRC2)/bbcp_Pthread.h $(SRC3)/bbcp_Debug.h \
                           $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Platform.h \
                           $(SRC2)/bbcp_Headers.h $(SRC2)/bbcp_Timer.h
	@echo Compiling bbcp_LogFile.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_MD5.o: $(SRC3)/bbcp_MD5.C $(SRC3)/bbcp_MD5.h \
                      $(SRC3)/bbcp_ChkSum.h $(SRC3)/bbcp_Endian.h
	@echo Compiling bbcp_MD5.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_NetLogger.o: $(SRC3)/bbcp_NetLogger.C $(SRC3)/bbcp_NetLogger.h \
						    $(SRC3)/NetLogger.h
	@echo Compiling bbcp_Netlogger.c
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Network.o:  $(SRC2)/bbcp_Network.C $(SRC2)/bbcp_Network.h \
                           $(SRC3)/bbcp_Debug.h $(SRC3)/bbcp_Emsg.h \
                           $(SRC2)/bbcp_Link.h $(SRC2)/bbcp_Pthread.h \
                           $(SRC2)/bbcp_Platform.h $(SRC2)/bbcp_Config.h
	@echo Compiling bbcp_Network.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Node.o: $(SRC2)/bbcp_Node.C $(SRC2)/bbcp_Node.h \
                       $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Emsg.h \
                       $(SRC3)/bbcp_Debug.h $(SRC2)/bbcp_File.h \
                       $(SRC2)/bbcp_FileSpec.h $(SRC2)/bbcp_Link.h \
                       $(SRC2)/bbcp_BuffPool.h $(SRC2)/bbcp_Network.h \
                       $(SRC2)/bbcp_Protocol.h $(SRC2)/bbcp_ProcMon.h \
                       $(SRC2)/bbcp_ProgMon.h $(SRC2)/bbcp_Stream.h \
                       $(SRC2)/bbcp_System.h $(SRC3)/bbcp_ZCX.h \
                       $(SRC2)/bbcp_Headers.h $(SRC1)/MDTMApp_TaskGroup.h
	@echo Compiling bbcp_Node.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_ProcMon.o: $(SRC2)/bbcp_ProcMon.C $(SRC2)/bbcp_ProcMon.h \
                          $(SRC3)/bbcp_Debug.h $(SRC3)/bbcp_Emsg.h \
                          $(SRC2)/bbcp_File.h $(SRC2)/bbcp_Pthread.h \
                          $(SRC2)/bbcp_Headers.h
	@echo Compiling bbcp_ProcMon.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_ProgMon.o: $(SRC2)/bbcp_ProgMon.C $(SRC2)/bbcp_ProgMon.h \
                          $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                          $(SRC2)/bbcp_File.h $(SRC2)/bbcp_Headers.h \
                          $(SRC2)/bbcp_Pthread.h $(SRC2)/bbcp_Timer.h \
                          $(SRC3)/bbcp_ZCX.h
	@echo Compiling bbcp_ProgMon.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Protocol.o: $(SRC2)/bbcp_Protocol.C $(SRC2)/bbcp_Protocol.h \
                           $(SRC2)/bbcp_Config.h $(SRC2)/bbcp_Node.h \
                           $(SRC2)/bbcp_FileSpec.h $(SRC2)/bbcp_FileSystem.h \
                           $(SRC2)/bbcp_IO.h $(SRC2)/bbcp_File.h \
                           $(SRC2)/bbcp_Link.h $(SRC3)/bbcp_Debug.h \
                           $(SRC2)/bbcp_Pthread.h $(SRC2)/bbcp_Stream.h \
                           $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Network.h \
                           $(SRC3)/bbcp_Version.h $(SRC2)/bbcp_Headers.h
	@echo Compiling bbcp_Protocol.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Pthread.o: $(SRC2)/bbcp_Pthread.C $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_Pthread.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_RTCopy.o: $(SRC3)/bbcp_RTCopy.C $(SRC3)/bbcp_RTCopy.h \
                         $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Emsg.h \
                         $(SRC2)/bbcp_FileSystem.h $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_RTCopy.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Stream.o: $(SRC2)/bbcp_Stream.C $(SRC2)/bbcp_Stream.h \
                         $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                         $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_System.h
	@echo Compiling bbcp_Stream.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_System.o: $(SRC2)/bbcp_System.C $(SRC2)/bbcp_System.h \
                         $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h \
                         $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Platform.h \
                         $(SRC2)/bbcp_Pthread.h
	@echo Compiling bbcp_System.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Timer.o: $(SRC2)/bbcp_Timer.C $(SRC2)/bbcp_Timer.h
	@echo Compiling bbcp_Timer.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC2)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_Version.o: $(SRC3)/bbcp_Version.C $(SRC3)/bbcp_Version.h \
                          $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Emsg.h
	@echo Compiling bbcp_Version.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/MDTMApp_Network.o: $(SRC1)/MDTMApp_Network.C $(SRC1)/MDTMApp_Network.h\
                             $(SRC1)/MDTMApp.h $(SRC1)/MDTMApp_TaskGroup.h \
                             $(SRC2)/bbcp_FileSpec.h $(SRC3)/bbcp_Emsg.h \
                             $(SRC2)/bbcp_Stream.h $(SRC1)/MDTMApp_Daemon.h \
                             $(SRC2)/bbcp_Pthread.h $(SRC2)/bbcp_System.h
	@echo Compiling MDTMApp_Network.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC1)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/MDTMApp_TaskGroup.o: $(SRC1)/MDTMApp_TaskGroup.C \
                               $(SRC1)/MDTMApp_TaskGroup.h \
                               $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp.h \
                               $(SRC2)/bbcp_Node.h $(SRC2)/bbcp_FileSpec.h \
                               $(SRC3)/bbcp_Emsg.h $(SRC2)/bbcp_Stream.h \
                               $(SRC1)/MDTMApp_Daemon.h $(SRC2)/bbcp_Pthread.h \
                               $(SRC2)/bbcp_System.h
	@echo Compiling MDTMApp_TaskGroup.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC1)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/MDTMApp_Daemon.o: $(SRC1)/MDTMApp_Daemon.C $(SRC1)/MDTMApp_Daemon.h \
                            $(SRC1)/MDTMApp_TaskGroup.h \
                            $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp.h \
                            $(SRC2)/bbcp_FileSpec.h $(SRC3)/bbcp_Emsg.h \
                            $(SRC2)/bbcp_Node.h
	@echo Compiling MDTMApp_Daemon.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC1)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/bbcp_ZCX.o: $(SRC3)/bbcp_ZCX.C $(SRC3)/bbcp_ZCX.h \
                      $(SRC2)/bbcp_BuffPool.h $(SRC3)/bbcp_Emsg.h
	@echo Compiling bbcp_ZCX.C
	@$(CC) -c $(CFLAGS) $(INCLUDE) $(SRC3)/$(*F).C -o $(OBJDIR)/$(*F).o

$(OBJDIR)/NetLogger.o: $(SRC3)/NetLogger.c $(SRC3)/NetLogger.h
	@echo Compiling NetLogger.c
	@$(BB) -c $(BFLAGS) $(INCLUDE) $(SRC3)/$(*F).c -o $(OBJDIR)/$(*F).o

# DO NOT DELETE

mdtmAppA.o: /usr/include/unistd.h /usr/include/features.h
mdtmAppA.o: /usr/include/sys/cdefs.h /usr/include/bits/wordsize.h
mdtmAppA.o: /usr/include/gnu/stubs.h /usr/include/gnu/stubs-64.h
mdtmAppA.o: /usr/include/bits/posix_opt.h /usr/include/bits/environments.h
mdtmAppA.o: /usr/include/bits/types.h /usr/include/bits/typesizes.h
mdtmAppA.o: /usr/include/bits/confname.h /usr/include/getopt.h
mdtmAppA.o: /usr/include/ctype.h /usr/include/endian.h /usr/include/bits/endian.h
mdtmAppA.o: /usr/include/bits/byteswap.h /usr/include/xlocale.h
mdtmAppA.o: /usr/include/errno.h /usr/include/bits/errno.h
mdtmAppA.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
mdtmAppA.o: /usr/include/asm-generic/errno.h
mdtmAppA.o: /usr/include/asm-generic/errno-base.h /usr/include/stdlib.h
mdtmAppA.o: /usr/include/bits/waitflags.h /usr/include/bits/waitstatus.h
mdtmAppA.o: /usr/include/sys/types.h /usr/include/time.h
mdtmAppA.o: /usr/include/sys/select.h /usr/include/bits/select.h
mdtmAppA.o: /usr/include/bits/sigset.h /usr/include/bits/time.h
mdtmAppA.o: /usr/include/sys/sysmacros.h /usr/include/bits/pthreadtypes.h
mdtmAppA.o: /usr/include/alloca.h /usr/include/string.h /usr/include/strings.h
mdtmAppA.o: /usr/include/stdio.h /usr/include/libio.h /usr/include/_G_config.h
mdtmAppA.o: /usr/include/wchar.h /usr/include/bits/stdio_lim.h
mdtmAppA.o: /usr/include/bits/sys_errlist.h /usr/include/sys/param.h
mdtmAppA.o: /usr/include/limits.h /usr/include/bits/posix1_lim.h
mdtmAppA.o: /usr/include/bits/local_lim.h /usr/include/linux/limits.h
mdtmAppA.o: /usr/include/bits/posix2_lim.h /usr/include/linux/param.h
mdtmAppA.o: /usr/include/asm/param.h /usr/include/asm-generic/param.h bbcp_Args.h
mdtmAppA.o: $(SRC2)/bbcp_Stream.h $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h $(SRC2)/bbcp_Headers.h
mdtmAppA.o: $(SRC2)/bbcp_FileSpec.h /usr/include/utime.h $(SRC2)/bbcp_FileSystem.h
mdtmAppA.o: /usr/include/sys/stat.h /usr/include/bits/stat.h $(SRC3)/bbcp_LogFile.h
mdtmAppA.o: /usr/include/sys/uio.h /usr/include/bits/uio.h $(SRC2)/bbcp_Timer.h
mdtmAppA.o: /usr/include/sys/time.h $(SRC2)/bbcp_Pthread.h /usr/include/pthread.h
mdtmAppA.o: /usr/include/sched.h /usr/include/bits/sched.h
mdtmAppA.o: /usr/include/bits/setjmp.h /usr/include/semaphore.h
mdtmAppA.o: /usr/include/bits/semaphore.h $(SRC2)/bbcp_Node.h $(SRC2)/bbcp_File.h $(SRC2)/bbcp_BuffPool.h
mdtmAppA.o: $(SRC2)/bbcp_IO.h $(SRC2)/bbcp_Link.h /usr/include/fcntl.h /usr/include/bits/fcntl.h
mdtmAppA.o: $(SRC3)/bbcp_ChkSum.h $(SRC2)/bbcp_ProcMon.h $(SRC2)/bbcp_Protocol.h $(SRC2)/bbcp_System.h $(SRC1)/MDTMApp_TaskGroup.h $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp.h $(SRC1)/MDTMApp_Daemon.h
mdtmAppD.o: /usr/include/unistd.h /usr/include/features.h
mdtmAppD.o: /usr/include/sys/cdefs.h /usr/include/bits/wordsize.h
mdtmAppD.o: /usr/include/gnu/stubs.h /usr/include/gnu/stubs-64.h
mdtmAppD.o: /usr/include/bits/posix_opt.h /usr/include/bits/environments.h
mdtmAppD.o: /usr/include/bits/types.h /usr/include/bits/typesizes.h
mdtmAppD.o: /usr/include/bits/confname.h /usr/include/getopt.h
mdtmAppD.o: /usr/include/ctype.h /usr/include/endian.h /usr/include/bits/endian.h
mdtmAppD.o: /usr/include/bits/byteswap.h /usr/include/xlocale.h
mdtmAppD.o: /usr/include/errno.h /usr/include/bits/errno.h
mdtmAppD.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
mdtmAppD.o: /usr/include/asm-generic/errno.h
mdtmAppD.o: /usr/include/asm-generic/errno-base.h /usr/include/stdlib.h
mdtmAppD.o: /usr/include/bits/waitflags.h /usr/include/bits/waitstatus.h
mdtmAppD.o: /usr/include/sys/types.h /usr/include/time.h
mdtmAppD.o: /usr/include/sys/select.h /usr/include/bits/select.h
mdtmAppD.o: /usr/include/bits/sigset.h /usr/include/bits/time.h
mdtmAppD.o: /usr/include/sys/sysmacros.h /usr/include/bits/pthreadtypes.h
mdtmAppD.o: /usr/include/alloca.h /usr/include/string.h /usr/include/strings.h
mdtmAppD.o: /usr/include/stdio.h /usr/include/libio.h /usr/include/_G_config.h
mdtmAppD.o: /usr/include/wchar.h /usr/include/bits/stdio_lim.h
mdtmAppD.o: /usr/include/bits/sys_errlist.h /usr/include/sys/param.h
mdtmAppD.o: /usr/include/limits.h /usr/include/bits/posix1_lim.h
mdtmAppD.o: /usr/include/bits/local_lim.h /usr/include/linux/limits.h
mdtmAppD.o: /usr/include/bits/posix2_lim.h /usr/include/linux/param.h
mdtmAppD.o: /usr/include/asm/param.h /usr/include/asm-generic/param.h bbcp_Args.h
mdtmAppD.o: $(SRC2)/bbcp_Stream.h $(SRC2)/bbcp_Config.h $(SRC3)/bbcp_Debug.h $(SRC2)/bbcp_Headers.h
mdtmAppD.o: $(SRC2)/bbcp_FileSpec.h /usr/include/utime.h $(SRC2)/bbcp_FileSystem.h
mdtmAppD.o: /usr/include/sys/stat.h /usr/include/bits/stat.h $(SRC3)/bbcp_LogFile.h
mdtmAppD.o: /usr/include/sys/uio.h /usr/include/bits/uio.h $(SRC2)/bbcp_Timer.h
mdtmAppD.o: /usr/include/sys/time.h $(SRC2)//bbcp_Pthread.h /usr/include/pthread.h
mdtmAppD.o: /usr/include/sched.h /usr/include/bits/sched.h
mdtmAppD.o: /usr/include/bits/setjmp.h /usr/include/semaphore.h
mdtmAppD.o: /usr/include/bits/semaphore.h $(SRC2)/bbcp_Node.h $(SRC2)/bbcp_File.h $(SRC2)/bbcp_BuffPool.h
mdtmAppD.o: $(SRC2)/bbcp_IO.h $(SRC2)/bbcp_Link.h /usr/include/fcntl.h /usr/include/bits/fcntl.h
mdtmAppD.o: $(SRC3)/bbcp_ChkSum.h $(SRC2)/bbcp_ProcMon.h $(SRC2)/bbcp_Protocol.h $(SRC2)/bbcp_System.h $(SRC1)/MDTMApp_Network.h $(SRC1)/MDTMApp.h $(SRC1)/MDTMApp_TaskGroup.h $(SRC1)/MDTMApp_Daemon.h

